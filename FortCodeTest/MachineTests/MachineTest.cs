﻿using NUnit.Framework;
using Ex1 = FortCodeExercises.Exercise1; 

namespace FortCodeTest.MachineTests
{
   [TestFixture]
   class MachineTest
   {
      [SetUp]
      public void Setup()
      {
      }

      #region Instantiation
      [Test]
      [Category("Instantiation")]
      public void Verfity_Machine_Instantiates_With_Defualt_Values()
      {
         string new_machine_name_0 = "bulldozer";
         string new_machine_name_1 = "crane";

         Ex1.Machine cur_machine = new Ex1.Machine();
         Assert.False(cur_machine == null);
         Assert.True(string.IsNullOrEmpty(cur_machine.machineName));
         Assert.AreEqual(0, cur_machine.type);

         // This test is defaulting to "bulldozer" because the originl code
         // set machine_type to zero by default
         Ex1.IMachine new_machine_a = new Ex1.MachineNew();
         Assert.False(new_machine_a == null);
         Assert.AreEqual(new_machine_name_0, new_machine_a.Name);
         Assert.AreEqual(0, new_machine_a.MachineType);

         Ex1.IMachine new_machine_b = new Ex1.MachineNew(1);
         Assert.False(new_machine_b == null);
         Assert.AreEqual(new_machine_name_1, new_machine_b.Name);
         Assert.AreEqual(1, new_machine_b.MachineType);
      }
      #endregion

      #region Name
      [TestCaseSource(nameof(MachineNameCases))]
      [Category("Name")]
      public void Set_Original_Machine_Name_By_Type(int machine_type, string machine_name)
      {
         // original Machine returns expected name for type
         Ex1.Machine cur_machine = new Ex1.Machine();
         cur_machine.type = machine_type;
         Assert.AreEqual(machine_name, cur_machine.name);
      }

      [TestCaseSource(nameof(MachineNameCases))]
      [Category("Name")]
      public void Set_New_Machine_Name_By_Type_In_Ctor(int machine_type, string machine_name)
      {
         Ex1.MachineNew new_machine = new Ex1.MachineNew(machine_type);
         Assert.AreEqual(machine_name, new_machine.Name);
      }

      [TestCaseSource(nameof(MachineNameCases))]
      [Category("Name")]
      public void Set_New_Machine_Name_By_Type_Name_By_Property(int machine_type, string machine_name)
      {
         // new Machine returns expected name when name and type are set via property 
         Ex1.MachineNew new_machine = new Ex1.MachineNew();
         new_machine.MachineType = machine_type;
         Assert.AreEqual(machine_name, new_machine.Name);
      }

      [TestCaseSource(nameof(MachineNameCases))]
      [Category("Name")]
      public void Set_New_Machine_Name_By_Just_Type_By_Property(int machine_type, string machine_name)
      {
         // new Machine returns expected name when name and type are specified in Ctor,
         // and updated via property
         Ex1.MachineNew new_machine = new Ex1.MachineNew();
         new_machine.MachineType = machine_type;
         Assert.AreEqual(machine_name, new_machine.Name);
      }

      [TestCaseSource(nameof(MachineNameCases))]
      [Category("Name")]
      public void Set_New_Machine_Name_By_Type__Name_In_Ctor_Change_By_Property(int machine_type, string machine_name)
      {
         Ex1.IMachine new_machine = new Ex1.MachineNew(machine_type + 1);
         new_machine.MachineType = machine_type;
         Assert.AreEqual(machine_name, new_machine.Name);
      }
      #endregion

      #region Description
      [TestCaseSource(nameof(MachineDescriptionCases))]
      [Category("Description")]
      public void Set_Original_Machine_Description_By_Type(int machine_type, string machine_description)
      {
         Ex1.Machine cur_machine = new Ex1.Machine();
         cur_machine.type = machine_type;
         Assert.AreEqual(machine_description, cur_machine.description);
      }

      [TestCaseSource(nameof(MachineDescriptionCases))]
      [Category("Description")]
      public void Set_New_Machine_Description_By_Type(int machine_type, string machine_description)
      {
         Ex1.IMachine new_machine_a = new Ex1.MachineNew();
         new_machine_a.MachineType = machine_type;
         Assert.AreEqual(machine_description, new_machine_a.Description);

         Ex1.IMachine new_machine_b = new Ex1.MachineNew(machine_type);
         Assert.AreEqual(machine_description, new_machine_b.Description);
      }
      #endregion

      #region Color 
      [TestCaseSource(nameof(MachineColorCases))]
      [Category("Color")]
      public void Set_Original_Machine_Color_By_Type(int machine_type, string machine_color)
      {
         Ex1.Machine cur_machine = new Ex1.Machine();
         cur_machine.type = machine_type;
         Assert.AreEqual(machine_color, cur_machine.color);
      }
      
      [TestCaseSource(nameof(MachineColorCases))]
      [Category("Color")]
      public void Set_New_Machine_Color_By_Type(int machine_type, string machine_color)
      {
         Ex1.IMachine cur_machine = new Ex1.MachineNew();
         cur_machine.MachineType = machine_type;
         Assert.AreEqual(machine_color, cur_machine.Color);
      }
      #endregion

      #region TrimColor
      [TestCaseSource(nameof(MachineTrimColorCases))]
      [Category("TrimColor")]
      public void Set_Original_Machine_TrimColor_By_Type(int machine_type, string machine_trimcolor)
      {
         Ex1.Machine cur_machine = new Ex1.Machine();
         cur_machine.type = machine_type;
         Assert.AreEqual(machine_trimcolor, cur_machine.trimColor);
      }

      [TestCaseSource(nameof(MachineTrimColorCases))]
      [Category("TrimColor")]
      public void Set_New_Machine_TrimColor_By_Type(int machine_type, string machine_trimcolor)
      {
         Ex1.IMachine cur_machine = new Ex1.MachineNew();
         cur_machine.MachineType = machine_type;
         Assert.AreEqual(machine_trimcolor, cur_machine.TrimColor);
      }
      #endregion

      #region IsDark
      [TestCaseSource(nameof(MachineIsDarkCases))]
      [Category("IsDark")]
      public void Get_Original_Machine_IsDark_By_Color(string in_color, bool expected_result)
      {
         Ex1.Machine cur_machine = new Ex1.Machine();
         Assert.AreEqual(expected_result, cur_machine.isDark(in_color));
      }

      [TestCaseSource(nameof(MachineIsDarkCases))]
      [Category("IsDark")]
      public void Get_New_Machine_IsDark_By_Color(string in_color, bool expected_result)
      {
         Ex1.IMachine cur_machine = new Ex1.MachineNew();
         Assert.AreEqual(expected_result, cur_machine.IsDark(in_color));
      }
      #endregion

      #region MaxSpeed
      [TestCaseSource(nameof(MachineMaxSpeedCases))]
      [Category("MaxSpeed")]
      public void Get_Original_Machine_Max_Speed_By_Type_and_NoMax(int machine_type, bool hasNoMax, int expected_max_speed)
      {
         Ex1.Machine cur_machine = new Ex1.Machine();
         Assert.AreEqual(expected_max_speed, cur_machine.getMaxSpeed(machine_type, hasNoMax));
      }

      [TestCaseSource(nameof(MachineMaxSpeedCases))]
      [Category("MaxSpeed")]
      public void Get_New_Machine_Max_Speed_By_Type_and_NoMax(int machine_type, bool hasNoMax, int expected_max_speed)
      {
         Ex1.IMachine cur_machine = new Ex1.MachineNew();
         cur_machine.MachineType = machine_type;
         Ex1.MaxSpeedLimiter msl = hasNoMax == true ? Ex1.MaxSpeedLimiter.disabled : Ex1.MaxSpeedLimiter.enabled;
         Assert.AreEqual(expected_max_speed, cur_machine.GetMaxSpeed(machine_type, msl));
      }
      #endregion

      #region Common Data 

      private static object[] MachineNameCases =
      {
         new object[] {0,"bulldozer"},
         new object[] {1,"crane"},
         new object[] {2,"tractor"},
         new object[] {3,"truck"},
         new object[] {4,"car"},
         new object[] {5,""},
         new object[] {-1,""}
      };

      private static object[] MachineDescriptionCases =
      {

        new object[] {0, " red bulldozer [80]."},
        new object[] {1, " blue crane [75]."},
        new object[] {2, " green tractor [90]."},
        new object[] {3, " yellow truck [70]."},
        new object[] {4, " brown car [70]."},
        new object[] {5, " white  [70]."},
        new object[] {-1, " white  [70]."}
      };


      private static object[] MachineColorCases =
      {
        new object[] {0, "red"},
        new object[] {1, "blue"},
        new object[] {2, "green"},
        new object[] {3, "yellow"},
        new object[] {4, "brown"},
        new object[] {5, "white"},
        new object[] {-1, "white"}
      };

      private static object[] MachineMaxSpeedCases =
      {
         new object[] {0, true, 80},
         new object[] {0, false, 70},
         new object[] {-1, true, 70},
         new object[] {-1, false, 70},
         new object[] {1, true, 75},
         new object[] {1, false, 70},
         new object[] {2, true, 90},
         new object[] {2, false, 60},
         new object[] {3, true, 70},
         new object[] {3, false, 70},
         new object[] {4, true, 90},
         new object[] {4, false, 70},
         new object[] {5, true, 70},
         new object[] {5, false, 70},
         new object[] {100, true, 70},
         new object[] {100, false, 70}
      };

      private static object[] MachineTrimColorCases =
      {
         new object[] {0, ""},
         new object[] {1, "white"},
         new object[] {2, "gold"},
         new object[] {3, ""},
         new object[] {4, ""},
         new object[] {5, ""},
         new object[] {-1, ""}
      };

      private static object[] MachineIsDarkCases =
      {
        new object[] {"red", true},
        new object[] {"yellow", false},
        new object[] {"green", true},
        new object[] {"black", true},
        new object[] {"white", false},
        new object[] {"beige", false},
        new object[] {"babyblue", false},
        new object[] {"crimson", true}
      };
    
      #endregion
   }
}


