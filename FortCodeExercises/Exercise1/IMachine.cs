﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
   // put here for convenience
   public enum MaxSpeedLimiter
   {
      enabled,
      disabled
   }
   public interface IMachine
   {
      int MachineType
      {
         get;
         set; 
      }
      string Name
      {
         get;
      }

      string Description
      {
         get;
      }

      string Color
      {
         get;
      }

      string TrimColor
      {
         get;
      }

      bool IsDark(string color);

      int GetMaxSpeed(int machine_type, MaxSpeedLimiter maxSpeedEnabled);
   }
}
