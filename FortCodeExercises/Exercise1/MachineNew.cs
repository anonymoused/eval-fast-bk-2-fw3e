﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FortCodeExercises.Exercise1
{
   public class MachineNew : IMachine
   {
      #region Ctor, Members

      private int _machine_type;
      private Dictionary<int, Dictionary<MachineDataKeys, string>> _data;
      private Dictionary<MachineDataKeys, string> _default_data;

      private enum MachineDataKeys
      {
         Name,
         Color,
         BaseColor,
         MaxSpeedLimitedForType,
         MaxSpeedUnconrolled,
         MaxSpeedIsLimited,
         DarkTrimColor,
         LightTrimColor
      }

      public MachineNew()
      {
         InitializeData();
         _machine_type = 0;

      }
      public MachineNew(int machineType)
      {
         InitializeData();
         _machine_type = machineType;
      }

      #endregion

      #region Getters Setters

      public int MachineType
      {
         get => _machine_type;
         set { _machine_type = value; }
      }

      public string Name
      {
         get => GetName();
      }

      public string Color
      {
         get => GetColor();
      }

      public string Description
      {
         get => GetDescription();
      }
      public string TrimColor
      {
         get => GetTrimColor();
      }

      public bool IsDark(string color)
      {
         switch(color)
         {
            case ("red"): 
               return true;
            case ("yellow"):
               return false;
            case ("green"):
               return true;
            case ("black"):
               return true;
            case ("white"):
               return false;
            case ("beige"):
               return false;
            case ("babyblue"):
               return false;
            case ("crimson"):
               return true;
            default:
               return false;
         }
      }

      public int GetMaxSpeed(int machine_type, MaxSpeedLimiter MaxSpeedLimitierEnabledStatus)
      {
         string maxSpeedAsString = GetDataByKey(machine_type, GetKeyForMaxSpeedLimitierStatus(MaxSpeedLimitierEnabledStatus));
         int.TryParse(maxSpeedAsString, out int maxSpeed);
         return maxSpeed;
      }

      #endregion

      #region Private Methods

      private string GetName()
      {
         return GetDataByKey(_machine_type, MachineDataKeys.Name);
      }
      private string GetDescription()
      {
         MaxSpeedLimiter maxSpeedLimiterStatus = MaxSpeedLimiter.disabled;
         if (String.Compare(GetDataByKey(_machine_type, MachineDataKeys.MaxSpeedLimitedForType), "true")  == 0)
            maxSpeedLimiterStatus = MaxSpeedLimiter.enabled;

         return $" {this.Color} {this.Name} [{this.GetMaxSpeed(_machine_type, maxSpeedLimiterStatus)}].";
      }
      private string GetColor()
      {
         return GetDataByKey(_machine_type, MachineDataKeys.Color);
      }
      private string GetTrimColor()
      {
         string baseColor = GetDataByKey(_machine_type, MachineDataKeys.BaseColor);

         if (IsDark(baseColor))
            return GetDataByKey(_machine_type, MachineDataKeys.DarkTrimColor);
         else
            return GetDataByKey(_machine_type, MachineDataKeys.LightTrimColor);
      }

      private string GetDataByKey(int outerKey, MachineDataKeys innerKey)
      {
         if (_data.ContainsKey(outerKey))
            return _data[outerKey][innerKey];
         else
            return _default_data[innerKey];
      }

      private MachineDataKeys GetKeyForMaxSpeedLimitierStatus(MaxSpeedLimiter maxSpeedLimiterStatus)
      {
         if (maxSpeedLimiterStatus == MaxSpeedLimiter.disabled)
            return MachineDataKeys.MaxSpeedUnconrolled;
         else
            return MachineDataKeys.MaxSpeedIsLimited;
      }

      private void InitializeData()
      {
         _data = new Dictionary<int, Dictionary<MachineDataKeys, string>>()
         {
            {0, new Dictionary<MachineDataKeys, string>
                {
                   {MachineDataKeys.Name,                   "bulldozer"},
                   {MachineDataKeys.Color,                  "red" },
                   {MachineDataKeys.BaseColor,              "red"},
                   {MachineDataKeys.DarkTrimColor,          ""},
                   {MachineDataKeys.LightTrimColor,         ""},
                   {MachineDataKeys.MaxSpeedLimitedForType, "false" },
                   {MachineDataKeys.MaxSpeedIsLimited,      "70" },
                   {MachineDataKeys.MaxSpeedUnconrolled,    "80" },
                } 
            },
            {1, new Dictionary<MachineDataKeys, string>
                {
                   {MachineDataKeys.Name,                   "crane"},
                   {MachineDataKeys.Color,                  "blue" },
                   {MachineDataKeys.BaseColor,              "blue"},
                   {MachineDataKeys.DarkTrimColor,          "black"},
                   {MachineDataKeys.LightTrimColor,         "white"},
                   {MachineDataKeys.MaxSpeedLimitedForType, "false" },
                   {MachineDataKeys.MaxSpeedIsLimited,      "70" },
                   {MachineDataKeys.MaxSpeedUnconrolled,     "75" }
                }
            },
            {2, new Dictionary<MachineDataKeys, string>
                {
                   {MachineDataKeys.Name,                   "tractor"},
                   {MachineDataKeys.Color,                  "green"},
                   {MachineDataKeys.BaseColor,              "green"},
                   {MachineDataKeys.DarkTrimColor,          "gold"},
                   {MachineDataKeys.LightTrimColor,         ""},
                   {MachineDataKeys.MaxSpeedLimitedForType, "false" },
                   {MachineDataKeys.MaxSpeedIsLimited,      "60" },
                   {MachineDataKeys.MaxSpeedUnconrolled,    "90" },
                }
             },
            {3, new Dictionary<MachineDataKeys, string>
                {
                   {MachineDataKeys.Name,                   "truck"},
                   {MachineDataKeys.Color,                  "yellow" },
                   {MachineDataKeys.BaseColor,              "yellow"},
                   {MachineDataKeys.DarkTrimColor,          "silver"},
                   {MachineDataKeys.LightTrimColor,         ""},
                   {MachineDataKeys.MaxSpeedLimitedForType, "true" },
                   {MachineDataKeys.MaxSpeedIsLimited,      "70" },
                   {MachineDataKeys.MaxSpeedUnconrolled,    "70" },
                }
            },
            {4, new Dictionary<MachineDataKeys, string>
                {
                   {MachineDataKeys.Name,                   "car"},
                   {MachineDataKeys.Color,                  "brown" },
                   {MachineDataKeys.BaseColor,              "brown"},
                   {MachineDataKeys.DarkTrimColor,          ""},
                   {MachineDataKeys.LightTrimColor,         ""},
                   {MachineDataKeys.MaxSpeedLimitedForType, "true" },
                   {MachineDataKeys.MaxSpeedIsLimited,       "70" },
                   {MachineDataKeys.MaxSpeedUnconrolled,     "90" },
                }
            },
         };

         _default_data = new Dictionary<MachineDataKeys, string>
         {
            {MachineDataKeys.Name,                   ""},
            {MachineDataKeys.Color,                  "white" },
            {MachineDataKeys.BaseColor,              "red"},
            {MachineDataKeys.DarkTrimColor,          ""},
            {MachineDataKeys.LightTrimColor,         ""},
            {MachineDataKeys.MaxSpeedLimitedForType, "true" },
            {MachineDataKeys.MaxSpeedIsLimited,      "70" },
            {MachineDataKeys.MaxSpeedUnconrolled,    "70" },
          };
      }

      #endregion
   }
}
